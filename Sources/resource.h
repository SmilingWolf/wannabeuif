#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define DLG_MAIN                                100
#define IDI_ICON1                               101
#define EDT_LOG                                 40000
#define EDT_STARTVA                             40001
#define BTN_EXIT                                40002
#define BTN_MAGIC                               40003
#define EDT_PID                                 40004
#define EDT_ENDVA                               40005
#define EDT_NEWVA                               40006
#define CHK_FASTSPEED                           40007
#define BTN_SAVELOG                             40008
